//Generated File.  Do not modify by hand.

using System;
using Server;

namespace Server
{
    public static class ServerVersion
    {
        public const int Major = 1;
        public const int Minor = 6;
        public const int Rev = 103;
    }
}