using System;
using Server;
using Server.Commands;
using Server.Items;
using Server.Mobiles;
using Server.Targeting;

namespace Server
{
    // this is the object that goes onto Mobile to track prots.
    // the Prot class (no 's') is what we attach to items to give them a single prot.
    public class Prots
    {
        public static void Initialize()
        {
            CommandSystem.Register("GetProts", AccessLevel.GameMaster, GetProts_OnCommand);
            CommandSystem.Register("Prots", AccessLevel.Player, Prots_OnCommand);
        }

        public static void Prots_OnCommand(CommandEventArgs e)
        {
            PlayerMobile pm = e.Mobile as PlayerMobile;
            pm.Prots.UpdateProts();
            pm.SendMessage("Air: {0}, Earth: {1}, Fire: {2}, Necro: {3}, Water: {4}, Poison: {5}", pm.Prots.Air,
                pm.Prots.Earth, pm.Prots.Fire, pm.Prots.Necro, pm.Prots.Water, pm.Prots.Poison);
        }

        public static void GetProts_OnCommand(CommandEventArgs e)
        {
            PlayerMobile pm = e.Mobile as PlayerMobile;

            pm.Target = new InternalTarget();
        }

        private Mobile _parent;

        public Prots(Mobile parent)
        {
            _parent = parent;
            Air = 0;
            Fire = 0;
            Earth = 0;
            Water = 0;
            Necro = 0;
            Poison = 0;
        }

        public int Fire { get; set; }

        public int Water { get; set; }

        public int Air { get; set; }

        public int Earth { get; set; }

        public int Necro { get; set; }

        public int Poison { get; set; }

        public void UpdateProts()
        {
            Air = PiecewiseScale(DamageType.Air);
            Earth = PiecewiseScale(DamageType.Earth);
            Fire = PiecewiseScale(DamageType.Fire);
            Necro = PiecewiseScale(DamageType.Necro);
            Water = PiecewiseScale(DamageType.Water);
            Poison = PiecewiseScale(DamageType.Poison);
        }

        //index of the matrix corresponds to a server.layer
        public static readonly double[] Scalars =
        {
            0.00, 0.1, 0.5, 0.05, 0.3, 0.4, 0.2, 0.1,
            0.3, 0.3, 0.3, 0.0, 0.4, 0.4, 0.3, 0.0,
            0.0, 0.4, 0.3, 0.2, 0.4, 0.0, 0.4, 0.3,
            0.3
        };

        private int PiecewiseScale(DamageType dt)
        {
            double tally = 0;

            foreach (Layer l in Enum.GetValues(typeof(Layer)))
            {
                if (l < Layer.FirstValid)
                {
                    continue;
                }

                if (l > Layer.LastUserValid)
                {
                    break;
                }

                Item item = _parent.FindItemOnLayer(l);

                if (item != null)
                {
                    if (item is BaseArmor || item is BaseJewel)
                    {
                        tally += (Scalars[(int) l] * Assess(item, dt));
                    }
                    else if (item is BaseClothing c)
                    {
                        if (c.Prot.Element == dt)
                        {
                            tally += (Scalars[(int) l] * c.Prot.Level);
                        }
                    }
                }
            }

            if (tally > 3)
            {
                tally = 3; //hard cap at 75% prot
            }
            /*if( tally > 0 && tally < 1.0 ){
            tally = 1.0;
            }*/

            return (int) tally;
        }

        private int Assess(Item item, DamageType damageType)
        {
            CraftResource cr;

            switch (item)
            {
                case BaseArmor armor:
                    cr = armor.Resource;
                    break;
                case BaseJewel jewel:
                    cr = jewel.Resource;
                    break;
                default:
                    return 0;
            }

            return GetResist(cr, damageType);
        }

        private int GetResist(CraftResource cr, DamageType damageType)
        {
            switch (damageType)
            {
                case DamageType.Air:
                    switch (cr)
                    {
                        case CraftResource.Azurite:
                            return 2;
                        case CraftResource.Goddess:
                            return 1;
                        case CraftResource.RadiantNimbusDiamond:
                            return 3;
                        case CraftResource.GoldenDragonLeather:
                            return 1;
                    }

                    break;
                case DamageType.Earth:
                    switch (cr)
                    {
                        case CraftResource.Destruction:
                        case CraftResource.Crystal:
                        case CraftResource.WyrmLeather:
                        case CraftResource.GoldenDragonLeather:
                            return 1;
                        case CraftResource.RadiantNimbusDiamond:
                            return 3;
                    }

                    break;
                case DamageType.Fire:
                    switch (cr)
                    {
                        case CraftResource.Lavarock:
                        case CraftResource.LavaLeather:
                        case CraftResource.WyrmLeather:
                            return 2;
                        case CraftResource.GoldenDragonLeather:
                        case CraftResource.DarkSableRuby:
                            return 3;
                    }

                    break;
                case DamageType.Necro:
                    switch (cr)
                    {
                        case CraftResource.SilverRock:
                        case CraftResource.LicheLeather:
                        case CraftResource.NecromancerLeather:
                        case CraftResource.BalronLeather:
                            return 1;
                        case CraftResource.Undead:
                        case CraftResource.Virginity:
                            return 2;
                        case CraftResource.RadiantNimbusDiamond:
                            return 3;
                    }

                    break;
                case DamageType.Water:
                    switch (cr)
                    {
                        case CraftResource.IceRock:
                        case CraftResource.Dripstone:
                            return 1;
                        case CraftResource.IceCrystalLeather:
                            return 2;
                        case CraftResource.EbonTwilightSapphire:
                            return 3;
                    }

                    break;
                case DamageType.None:
                    break;
                case DamageType.Poison:
                    break;
                default:
                    return 0;
            }

            return 0;
        }

        private class InternalTarget : Target
        {
            public InternalTarget() : base(12, false, TargetFlags.None)
            {
            }

            protected override void OnTarget(Mobile from, object o)
            {
                if (o is Mobile m)
                {
                    from.SendMessage("Air: {0}, Earth: {1}, Fire: {2}, Necro: {3}, Water: {4}", m.Prots.Air,
                        m.Prots.Earth, m.Prots.Fire, m.Prots.Necro, m.Prots.Water);
                }
                else
                {
                    from.SendMessage("Can only target Mobiles");
                }
            }
        }
    }

    public class Prot
    {
        public Prot() : this(DamageType.None, 0)
        {
        }

        public Prot(DamageType element, int level)
        {
            Level = level;
            Element = element;
        }

        [CommandProperty(AccessLevel.GameMaster)]
        public int Level { get; set; }

        [CommandProperty(AccessLevel.GameMaster)]
        public DamageType Element { get; set; }

        public void Serialize(GenericWriter w)
        {
            w.Write((int) 0); //version

            w.Write(Level);
            w.Write((int) Element);
        }

        public void Deserialize(GenericReader r)
        {
            int version = r.ReadInt();

            Level = r.ReadInt();
            Element = (DamageType) r.ReadInt();
        }
    }
}