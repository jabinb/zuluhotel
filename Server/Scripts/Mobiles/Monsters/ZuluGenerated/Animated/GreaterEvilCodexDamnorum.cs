

using System;
using System.Collections.Generic;
using Server;
using Server.Ethics;
using Server.Misc;
using Server.Items;
using static Server.Mobiles.CreatureProp;

namespace Server.Mobiles
{
    public class GreaterEvilCodexDamnorum : BaseCreature
    {
        static GreaterEvilCodexDamnorum() => CreatureProperties.Register<GreaterEvilCodexDamnorum>(new CreatureProperties
        {
            // cast_pct = 80,
            // count_casts = 0,
            // CProp_AttackTypeImmunities = i256,
            // CProp_BaseHpRegen = i350,
            // CProp_BaseManaRegen = i500,
            // CProp_EarthProtection = i3,
            // CProp_massCastRange = i15,
            // CProp_NecroProtection = i8,
            // CProp_PermMagicImmunity = i6,
            // CProp_Permmr = i4,
            // DataElementId = GreaterEvilCodexDamnorum,
            // DataElementType = NpcTemplate,
            // dstart = 10,
            // Equip = EvilCodexDamnorum,
            // Graphic = 0x0ec4 /* Weapon */,
            // Hitscript = :combat:piercingscript /* Weapon */,
            // HitSound = 0x263 /* Weapon */,
            // hostile = 1,
            // lootgroup = 141,
            // MagicItemChance = 75,
            // MagicItemLevel = 6,
            // MissSound = 0x264 /* Weapon */,
            // num_casts = 20,
            // script = spellkillpcsTeleporter,
            // speech = 35,
            // Speed = 30 /* Weapon */,
            // spell = MassCast	darkness,
            // spell_0 = MassCast	decayingray,
            // spell_1 = MassCast	spectretouch,
            // spell_2 = MassCast	sorcerersbane,
            // spell_3 = MassCast	wyvernstrike,
            // spell_4 = MassCast	kill,
            // spell_5 = MassCast	plague,
            // spell_6 = teletoplayer,
            // Swordsmanship = 150,
            // TrueColor = 1645,
            AiType = AIType.AI_Mage /* spellkillpcsTeleporter */,
            AlwaysMurderer = true,
            BardImmune = true,
            Body = 0x3d9,
            CorpseNameOverride = "corpse of a Greater Evil Codex Damnorum",
            CreatureType = CreatureType.Animated,
            DamageMax = 65,
            DamageMin = 25,
            Dex = 1600,
            Female = false,
            FightMode = FightMode.Closest,
            FightRange = 1,
            HitsMax = 1850,
            Hue = 1645,
            Int = 1910,
            ManaMaxSeed = 1600,
            Name = "a Greater Evil Codex Damnorum",
            PerceptionRange = 10,
            PreferredSpells = new List<Type>
            {
                typeof(Spells.Necromancy.DarknessSpell),
                typeof(Spells.Necromancy.DecayingRaySpell),
                typeof(Spells.Necromancy.SpectresTouchSpell),
                typeof(Spells.Necromancy.SorcerorsBaneSpell),
                typeof(Spells.Necromancy.WyvernStrikeSpell),
                typeof(Spells.Necromancy.WyvernStrikeSpell),
                typeof(Spells.Necromancy.PlagueSpell),
            },
            Resistances = new Dictionary<ResistanceType, CreatureProp>
            {
                { ResistanceType.Poison, 100 },
                { ResistanceType.Energy, 75 },
                { ResistanceType.Cold, 75 },
                { ResistanceType.Fire, 75 },
            },
            SaySpellMantra = true,
            Skills = new Dictionary<SkillName, CreatureProp>
            {
                { SkillName.Parry, 100 },
                { SkillName.MagicResist, 120 },
                { SkillName.Tactics, 100 },
                { SkillName.Magery, 180 },
                { SkillName.Healing, 100 },
            },
            StamMaxSeed = 600,
            Str = 1600,
            TargetAcquireExhaustion = true,
            VirtualArmor = 25,
  
        });

        [Constructable]
        public GreaterEvilCodexDamnorum() : base(CreatureProperties.Get<GreaterEvilCodexDamnorum>())
        {
            // Add customization here

            AddItem(new SkinningKnife
            {
                Movable = false,
                Name = "Evil Codex Damnorum Weapon",
                Speed = 30,
                Skill = SkillName.Swords,
                MaxHitPoints = 250,
                HitPoints = 250,
                HitSound = 0x263,
                MissSound = 0x264,
            });
  
  
        }

        public GreaterEvilCodexDamnorum(Serial serial) : base(serial) {}

  

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int) 0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}