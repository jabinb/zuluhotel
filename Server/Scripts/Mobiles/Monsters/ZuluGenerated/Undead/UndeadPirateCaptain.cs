

using System;
using System.Collections.Generic;
using Server;
using Server.Ethics;
using Server.Misc;
using Server.Items;
using static Server.Mobiles.CreatureProp;

namespace Server.Mobiles
{
    public class UndeadPirateCaptain : BaseCreature
    {
        static UndeadPirateCaptain() => CreatureProperties.Register<UndeadPirateCaptain>(new CreatureProperties
        {
            // cast_pct = 50,
            // DataElementId = undeadpirate2,
            // DataElementType = NpcTemplate,
            // dstart = 10,
            // Equip = undeadpirate2,
            // HitSound = 0x23C /* Weapon */,
            // hostile = 1,
            // lootgroup = 23,
            // MagicItemChance = 1,
            // MagicItemChance_0 = 10,
            // MagicItemLevel = 1,
            // MissSound = 0x23A /* Weapon */,
            // num_casts = 3,
            // script = spellkillpcs,
            // speech = 54,
            // Speed = 45 /* Weapon */,
            // spell = flamestrike,
            // spell_0 = ebolt,
            // spell_1 = lightning,
            // spell_2 = fireball,
            // spell_3 = explosion,
            // TrueColor = 0,
            ActiveSpeed = 0.2,
            AiType = AIType.AI_Mage /* spellkillpcs */,
            AlwaysMurderer = true,
            Body = 0x190,
            CorpseNameOverride = "corpse of <random>, undead pirate captain",
            CreatureType = CreatureType.Undead,
            DamageMax = 16,
            DamageMin = 4,
            Dex = 250,
            Female = false,
            FightMode = FightMode.Closest,
            FightRange = 1,
            HitsMax = 250,
            Hue = 0,
            Int = 300,
            ManaMaxSeed = 400,
            Name = "<random>, undead pirate captain",
            PassiveSpeed = 0.4,
            PerceptionRange = 10,
            PreferredSpells = new List<Type>
            {
                typeof(Spells.Sixth.EnergyBoltSpell),
                typeof(Spells.Fourth.LightningSpell),
                typeof(Spells.Third.FireballSpell),
                typeof(Spells.Sixth.ExplosionSpell),
            },
            Resistances = new Dictionary<ResistanceType, CreatureProp>
            {
                { ResistanceType.Poison, 100 },
            },
            SaySpellMantra = true,
            Skills = new Dictionary<SkillName, CreatureProp>
            {
                { SkillName.MagicResist, 60 },
                { SkillName.Tactics, 100 },
                { SkillName.Macing, 100 },
                { SkillName.Magery, 100 },
            },
            StamMaxSeed = 150,
            Str = 250,
            VirtualArmor = 40,
  
        });

        [Constructable]
        public UndeadPirateCaptain() : base(CreatureProperties.Get<UndeadPirateCaptain>())
        {
            // Add customization here

  
        }

        public UndeadPirateCaptain(Serial serial) : base(serial) {}

  

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int) 0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}