

using System;
using System.Collections.Generic;
using Server;
using Server.Ethics;
using Server.Misc;
using Server.Items;
using static Server.Mobiles.CreatureProp;

namespace Server.Mobiles
{
    public class Reaper : BaseCreature
    {
        static Reaper() => CreatureProperties.Register<Reaper>(new CreatureProperties
        {
            // CProp_EarthProtection = i4,
            // DataElementId = reaper,
            // DataElementType = NpcTemplate,
            // dstart = 10,
            // Equip = reaper,
            // Graphic = 0x0ec4 /* Weapon */,
            // HitSound = 0x1BD /* Weapon */,
            // hostile = 1,
            // lootgroup = 34,
            // MissSound = 0x239 /* Weapon */,
            // script = killpcs,
            // Speed = 20 /* Weapon */,
            // TrueColor = 0,
            ActiveSpeed = 0.2,
            AiType = AIType.AI_Melee /* killpcs */,
            AlwaysMurderer = true,
            Body = 0x2f,
            CorpseNameOverride = "corpse of a reaper",
            CreatureType = CreatureType.Plant,
            DamageMax = 50,
            DamageMin = 5,
            Dex = 110,
            Female = false,
            FightMode = FightMode.Aggressor,
            FightRange = 1,
            HitsMax = 210,
            Hue = 0,
            Int = 35,
            ManaMaxSeed = 25,
            Name = "a reaper",
            PassiveSpeed = 0.4,
            PerceptionRange = 10,
            Resistances = new Dictionary<ResistanceType, CreatureProp>
            {
                { ResistanceType.Poison, 100 },
                { ResistanceType.Cold, 100 },
            },
            Skills = new Dictionary<SkillName, CreatureProp>
            {
                { SkillName.Parry, 60 },
                { SkillName.Magery, 100 },
                { SkillName.Tactics, 100 },
                { SkillName.Macing, 150 },
                { SkillName.MagicResist, 75 },
            },
            StamMaxSeed = 100,
            Str = 210,
            VirtualArmor = 30,
  
        });

        [Constructable]
        public Reaper() : base(CreatureProperties.Get<Reaper>())
        {
            // Add customization here

            AddItem(new SkinningKnife
            {
                Movable = false,
                Name = "Reaper Weapon",
                Speed = 20,
                MaxHitPoints = 250,
                HitPoints = 250,
                HitSound = 0x1BD,
                MissSound = 0x239,
            });
  
  
        }

        public Reaper(Serial serial) : base(serial) {}

  

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int) 0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}