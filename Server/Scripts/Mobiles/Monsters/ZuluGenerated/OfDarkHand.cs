

using System;
using System.Collections.Generic;
using Server;
using Server.Ethics;
using Server.Misc;
using Server.Items;
using static Server.Mobiles.CreatureProp;

namespace Server.Mobiles
{
    public class OfDarkHand : BaseCreature
    {
        static OfDarkHand() => CreatureProperties.Register<OfDarkHand>(new CreatureProperties
        {
            // CProp_nomountatdeath = i1,
            // CProp_NoReactiveArmour = i1,
            // DataElementId = darkknightmounted,
            // DataElementType = NpcTemplate,
            // dstart = 10,
            // Equip = servantofcain,
            // guardignore = 1,
            // HitSound = 0x238 /* Weapon */,
            // hostile = 1,
            // lootgroup = 59,
            // MissSound = 0x233 /* Weapon */,
            // mount = 0x3e9f 1170,
            // script = killpcsTeleporter,
            // Speed = 15 /* Weapon */,
            // Swordsmanship = 200,
            // TrueColor = 0,
            AiType = AIType.AI_Melee /* killpcsTeleporter */,
            AlwaysMurderer = true,
            BardImmune = true,
            Body = 0x190,
            ClassLevel = 6,
            ClassSpec = SpecName.Warrior,
            CorpseNameOverride = "corpse of <random> of the Dark Hand",
            DamageMax = 35,
            DamageMin = 10,
            Dex = 300,
            Female = false,
            FightMode = FightMode.Aggressor,
            FightRange = 1,
            HitsMax = 300,
            Hue = 1172,
            Int = 210,
            ManaMaxSeed = 200,
            Name = "<random> of the Dark Hand",
            PerceptionRange = 10,
            Skills = new Dictionary<SkillName, CreatureProp>
            {
                { SkillName.Tactics, 150 },
                { SkillName.MagicResist, 80 },
            },
            StamMaxSeed = 200,
            Str = 500,
            TargetAcquireExhaustion = true,
  
        });

        [Constructable]
        public OfDarkHand() : base(CreatureProperties.Get<OfDarkHand>())
        {
            // Add customization here

  
        }

        public OfDarkHand(Serial serial) : base(serial) {}

  

        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int) 0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}