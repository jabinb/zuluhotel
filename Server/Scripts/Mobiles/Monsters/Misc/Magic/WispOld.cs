using System;
using System.Collections.Generic;
using Server;
using Server.Ethics;
using Server.Misc;
using Server.Items;
using static Server.Mobiles.CreatureProp;

namespace Server.Mobiles
{
    [CorpseName("a wisp corpse")]
    public class WispOld : BaseCreature
    {
        public static readonly CreatureProperties CreatureProperties = new CreatureProperties
        {
            // /* test */ InitialInnocent = true,
            CorpseNameOverride = "a wisp corpse",
            Name = "a wisp",
            Body = 58,
            BaseSoundID = 466,
            AiType = AIType.AI_Mage,
            FightMode = FightMode.Aggressor,
            PerceptionRange = 10,
            FightRange = 1,
            ActiveSpeed = 0.2,
            PassiveSpeed = 0.4,
            Str = Between(196, 225),
            Dex = Between(196, 225),
            Int = Between(196, 225),
            HitsMax = Between(118, 135),
            DamageMax = 17,
            DamageMin = 18,
            Fame = 4000,
            Karma = 0,
            VirtualArmor = 40,
            SpeechType = InhumanSpeech.Wisp,
            EthicAllegiance = Ethic.Hero,
            ReacquireDelay = TimeSpan.FromSeconds(1.0),
            OppositionGroup = OppositionGroup.FeyAndUndead,
            DamageType = new Dictionary<ResistanceType, CreatureProp>
            {
                {ResistanceType.Physical, 50},
                {ResistanceType.Energy, 50}
            },
            Resistances = new Dictionary<ResistanceType, CreatureProp>
            {
                {ResistanceType.Physical, Between(35, 45)},
                {ResistanceType.Fire, Between(20, 40)},
                {ResistanceType.Cold, Between(10, 30)},
                {ResistanceType.Poison, Between(5, 10)},
                {ResistanceType.Energy, Between(50, 70)},
            },
            Skills = new Dictionary<SkillName, CreatureProp>
            {
                {SkillName.EvalInt, 80.0},
                {SkillName.Magery, 80.0},
                {SkillName.MagicResist, 80.0},
                {SkillName.Tactics, 80.0},
                {SkillName.Wrestling, 80.0},
            },
        };
        
        [Constructable]
        public WispOld() : base(CreatureProperties)
        {
            if (Core.ML && Utility.RandomDouble() < .33)
                PackItem(Engines.Plants.Seed.RandomPeculiarSeed(3));

            AddItem(new LightSource());
        }
        
        public WispOld(Serial serial) : base(serial) {}
        
        public override void Serialize(GenericWriter writer)
        {
            base.Serialize(writer);
            writer.Write((int) 0);
        }

        public override void Deserialize(GenericReader reader)
        {
            base.Deserialize(reader);
            int version = reader.ReadInt();
        }
    }
}