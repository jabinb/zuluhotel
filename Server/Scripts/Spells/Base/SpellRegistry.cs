using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Spells
{
    public class SpellRegistry
    {
        private static Type[] m_Types = new Type[700];
        private static int m_Count;

        public static Type[] Types
        {
            get
            {
                m_Count = -1;
                return m_Types;
            }
        }

        //What IS this used for anyways.
        public static int Count
        {
            get
            {
                if (m_Count == -1)
                {
                    m_Count = 0;

                    foreach (var t in m_Types)
                        if (t != null)
                            ++m_Count;
                }

                return m_Count;
            }
        }

        private static readonly Dictionary<Type, int> m_IDsFromTypes = new Dictionary<Type, int>(m_Types.Length);

        public static Dictionary<int, SpecialMove> SpecialMoves { get; } = new Dictionary<int, SpecialMove>();

        public static int GetRegistryNumber(ISpell s)
        {
            return GetRegistryNumber(s.GetType());
        }

        public static int GetRegistryNumber(SpecialMove s)
        {
            return GetRegistryNumber(s.GetType());
        }

        public static int GetRegistryNumber(Type type)
        {
            if (m_IDsFromTypes.ContainsKey(type))
                return m_IDsFromTypes[type];

            return -1;
        }

        public static void Register(int spellId, Type type)
        {
            if (spellId < 0 || spellId >= m_Types.Length)
                return;

            if (m_Types[spellId] == null)
                ++m_Count;

            m_Types[spellId] = type;

            if (!m_IDsFromTypes.ContainsKey(type))
                m_IDsFromTypes.Add(type, spellId);

            if (type.IsSubclassOf(typeof(SpecialMove)))
            {
                SpecialMove spm = null;

                try
                {
                    spm = Activator.CreateInstance(type) as SpecialMove;
                }
                catch
                {
                }

                if (spm != null)
                    SpecialMoves.Add(spellId, spm);
            }
        }

        public static SpecialMove GetSpecialMove(int spellId)
        {
            if (spellId < 0 || spellId >= m_Types.Length)
                return null;

            var t = m_Types[spellId];

            if (t == null || !t.IsSubclassOf(typeof(SpecialMove)) || !SpecialMoves.ContainsKey(spellId))
                return null;

            return SpecialMoves[spellId];
        }

        private static object[] m_Params = new object[2];

        public static Spell NewSpell(int spellId, Mobile caster, Item scroll)
        {
            if (spellId < 0 || spellId >= m_Types.Length)
                return null;

            return NewSpell(m_Types[spellId], caster, scroll);
        }

        public static Spell NewSpell(Type t, Mobile caster, Item scroll)
        {
            if (!m_Types.Contains(t))
                return null;

            if (t != null && !t.IsSubclassOf(typeof(SpecialMove)))
            {
                m_Params[0] = caster;
                m_Params[1] = scroll;

                try
                {
                    return (Spell)Activator.CreateInstance(t, m_Params);
                }
                catch
                {
                }
            }

            return null;
        }

        public static T NewSpell<T>(Mobile caster, Item scroll) where T : Spell
        {
            return (T)NewSpell(typeof(T), caster, scroll);
        }

        private static string[] m_CircleNames = {
                "First",
                "Second",
                "Third",
                "Fourth",
                "Fifth",
                "Sixth",
                "Seventh",
                "Eighth",
                "Necromancy",
                "Chivalry",
                "Bushido",
                "Ninjitsu",
                "Earth"
            };

        public static Spell NewSpell(string name, Mobile caster, Item scroll)
        {
            foreach (var t1 in m_CircleNames)
            {
                var t = ScriptCompiler.FindTypeByFullName($"Server.Spells.{t1}.{name}");

                if (t != null && !t.IsSubclassOf(typeof(SpecialMove)))
                {
                    var param = new object[]
                    {
                        caster,
                        scroll
                    };

                    try
                    {
                        return (Spell)Activator.CreateInstance(t, param);
                    }
                    catch
                    {
                    }
                }
            }

            return null;
        }
    }
}
