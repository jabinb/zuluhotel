using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Server;
using Server.Network;

namespace RunZH.Server.Scripts.Misc
{
    public static class StartingLocations
    {
        private static CityInfo[] m_Cities;

        public static CityInfo[] Cities
        {
            get { return m_Cities ??= Load(); }
        }

        private static CityInfo[] Load(string fileName = "startinglocs.xml")
        {
            string path = Path.Combine("Data/Locations/", fileName);

            if (!File.Exists(path))
                throw new FileNotFoundException("Could not load starting locations", path);

            try
            {
                var cities = (from city in XElement.Load(path).Descendants("City")
                    select new CityInfo(
                        (string) city.Attribute("name"),
                        (string) city.Attribute("building"),
                        Utility.ToInt32(city.Attribute("description").Value),
                        Utility.ToInt32(city.Attribute("x").Value),
                        Utility.ToInt32(city.Attribute("y").Value),
                        Utility.ToInt32(city.Attribute("z").Value),
                        Map.Felucca
                    )).ToArray();

                return cities;
            }
            catch (Exception e)
            {
                throw new Exception($"Failed to load starting locations from {path}", e);
            }
        }
    }
}