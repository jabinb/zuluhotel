using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Server;
using Server.Mobiles;

namespace Server
{
    public class NameList
    {
        public const string RandomNamePlaceholder = "<random>";
        private string m_Type;
        private string[] m_List;

        public string Type
        {
            get { return m_Type; }
        }

        public string[] List
        {
            get { return m_List; }
        }

        public bool ContainsName(string name)
        {
            foreach (var t in m_List)
                if (name == t)
                    return true;

            return false;
        }

        public NameList(string type, XmlElement xml)
        {
            m_Type = type;
            m_List = xml.InnerText.Split(',');

            for (int i = 0; i < m_List.Length; ++i)
                m_List[i] = Utility.Intern(m_List[i].Trim());
        }

        public string GetRandomName()
        {
            return m_List.Length > 0 ? m_List[Utility.Random(m_List.Length)] : "";
        }

        public static NameList GetNameList(string type)
        {
            Table.TryGetValue(type, out var n);
            return n;
        }

        public static string RandomName(string type)
        {
            NameList list = GetNameList(type);

            if (list != null)
                return list.GetRandomName();

            return "";
        }

        public static void SubstituteCreatureName(BaseCreature c)
        {
            if (!c.Name.Contains(RandomNamePlaceholder) || !c.CorpseNameOverride.Contains(RandomNamePlaceholder))
                return;

            string value;
            try
            {
                var pair = Table.First(
                    kv => c.Name.Contains(kv.Key, StringComparison.InvariantCultureIgnoreCase) ||
                          c.GetType().Name.Contains(kv.Key, StringComparison.InvariantCultureIgnoreCase)
                );
                value = pair.Value.GetRandomName();
            }
            catch (InvalidOperationException)
            {
                value = RandomName("male");
            }

            c.Name = c.Name.Replace(RandomNamePlaceholder, value, StringComparison.InvariantCultureIgnoreCase);

            c.CorpseNameOverride = c.CorpseNameOverride.Replace(RandomNamePlaceholder, value,
                StringComparison.InvariantCultureIgnoreCase);
        }

        public static Dictionary<string, NameList> Table { get; }

        static NameList()
        {
            Table = new Dictionary<string, NameList>(StringComparer.OrdinalIgnoreCase);

            string filePath = Path.Combine(Core.BaseDirectory, "Data/names.xml");

            if (!File.Exists(filePath))
                return;

            try
            {
                Load(filePath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Warning: Exception caught loading name lists:");
                Console.WriteLine(e);
            }
        }

        private static void Load(string filePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filePath);

            XmlElement root = doc["names"];

            foreach (XmlElement element in root.GetElementsByTagName("namelist"))
            {
                string type = element.GetAttribute("type");

                if (String.IsNullOrEmpty(type))
                    continue;

                try
                {
                    NameList list = new NameList(type, element);

                    Table[type] = list;
                }
                catch
                {
                }
            }
        }
    }
}