using System;
using Server.Spells;
using Server.Spells.Earth;
using Server.Spells.Fourth;
using Server.Spells.Third;

namespace Server.Items
{
    public class TriElementalStrike : WeaponAbility
    {
        private static readonly Action<Mobile, Mobile>[] Spells = 
        {
             (attacker, defender) => Spell.Create<LightningSpell>(attacker, null, true).Target(defender),
             (attacker, defender) => Spell.Create<FireballSpell>(attacker, null, true).Target(defender),
             (attacker, defender) => Spell.Create<IceStrikeSpell>(attacker, null, true).Target(defender)
        };
        
        public override void OnHit(Mobile attacker, Mobile defender, int damage)
        {
            if (!Validate(attacker))
                return;

            ClearCurrentAbility(attacker);
            
            try
            {
                foreach (var castAction in Spells)
                {
                    castAction(attacker, defender);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Failed to invoke {GetType().Name}> for Creature: {attacker.GetType().Name}, Serial: {attacker.Serial}");
            }
        }
    }
}